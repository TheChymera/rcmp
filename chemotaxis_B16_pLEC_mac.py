# -*- coding: utf8 -*-

import itertools
import numpy as np
import pandas as pd
from os import path
from statsmodels.stats.multitest import multipletests

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'chemotaxis_B16_pLEC_mac.csv'), index_col=False)
df = df.dropna(axis=0,how='any')
df = df.groupby('Well').mean()

# Modelling
import statsmodels.api as sm
import statsmodels.formula.api as smf
formula = 'Value ~ C(Day) + Hypoxia*pLECs*J774'
model = smf.ols(formula, df)
fit = model.fit()
summary = fit.summary()
anova_summary = sm.stats.anova_lm(fit, typ=3)

# Write data
with open("chemotaxis_B16_pLEC_macGLM.txt", "w") as text_file:
    text_file.write(summary.as_text())
with open("chemotaxis_B16_pLEC_macANOVA.txt", "w") as text_file:
    text_file.write(anova_summary.to_string())

# Print output
print(anova_summary)
print(summary)

formula = 'Value ~ C(Day) + pLECs + Hypoxia:pLECs + pLECs:J774'
model = smf.ols(formula, df)
fit = model.fit()
summary_ = fit.summary()
levels = [
	'pLECs[False]:J774[T.True]',
	'pLECs[True]:J774[T.True]',
	]
comparisons = [[a,b] for a, b in itertools.combinations(levels,2)]
comparisons += levels
levels = [
	'Hypoxia[T.True]:pLECs[False]',
	'Hypoxia[T.True]:pLECs[True]',
	]
comparisons += [[a,b] for a, b in itertools.combinations(levels,2)]
comparisons += levels
f_contrast = ['{} - {}'.format(a[0],a[1]) if len(a)==2 else a for a in comparisons]
f_contrast = ','.join(f_contrast)
f = fit.f_test(f_contrast)

contrasts = []
for comparison in comparisons:
	if isinstance(comparison, str):
		contrast = [1 if i == comparison else 0 for i in fit.params.index.to_list()]
		contrasts.append(contrast)
	else:
		contrast = [int(i in comparison) for i in levels]
		contrast = [1 if i in comparison else 0 for i in fit.params.index.to_list()]
		second = False
		for ix, i in enumerate(contrast):
			if i==1 and second:
				break
			elif i == 1:
				second=True
		contrast[ix] = -1
		contrasts.append(contrast)

t_tests = fit.t_test(np.array(contrasts))

legend=''
g, corrected_pvalues, _, _ = multipletests(t_tests.pvalue, alpha=0.05, method='fdr_bh')
for ix, p in enumerate(corrected_pvalues):
	legend += 'c{} = {}:\n\tBenjamini-Hochberg corrected p={}\n'.format(ix,comparisons[ix],p)

with open("chemotaxis_B16_pLEC_macPosthoc.txt", "w") as text_file:
	text_file.seek(0)
	text_file.write(summary_.as_text())
	text_file.write('\n')
	text_file.write(f.__str__())
	text_file.write('\n\n')
	text_file.write(legend)
	text_file.write('\n')
	text_file.write(t_tests.__str__())
	text_file.write('\n')

# Print output
print(summary_)
print('\n')
print(f.__str__())
print('\n\n')
print(legend)
print('\n')
print(t_tests.__str__())
print('\n')
