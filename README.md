# Ramanil's Cell Migration Paper

## Statistical Methods

Modelling and statistical analysis – including Ordinary Least Squares (OLS) regression, Analysis of Variance (ANOVA), t-tests, and Benjamini-Hochberg multiple comparison correction – were performed using the free and open source software package, [Statsmodels](http://conference.scipy.org/proceedings/scipy2010/pdfs/seabold.pdf).
All code used to generate the associated top-level statistics (along with the top-level data) has been made [openly accessible](https://bitbucket.org/TheChymera/rcmp/src/master/).

## Modelling

Statistical models were fitted using per-well aggregates, with the repetition (day) of the experiment, endothelial cell presence, and hypoxia modelled as main effects.
Both interaction effects and main effects were modelled for the following factors pairs, depending on the data in question:

* Endothelial Cell Type and DMOG
* pLECs and J774
* pLECs and DMOG
* J774 and DMOG

If interaction effects were significant (p smaller or equal to 0.05), post-hoc t-tests were performed for the pairwise level combinations of the interaction term.
Multiple comparison correction was performed via the Benjamini-Hochberg method, fixing the false discovery rate (FDR) at 0.05.
